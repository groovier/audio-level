An audio peak level indicator written in Rust that runs in a terminal window.

It is verified to run on Windows 10 and Ubuntu 18.04.

# Color Legend

* Blue: signal level is too low
* Green: signal lavel is good
* Yellow: signal level is high
* Red: signal level is too high; risk of clipping
* Magenta: signal level is too high, and clipping occurs