use std::{io::{stdout, Stdout, Write}};
use std::time::{Instant, Duration};

use crossterm::{
    style::{Print, PrintStyledContent, Colorize},
    terminal::{Clear, ClearType},
    Result as CrosstermResult,
    queue,
    cursor,
};

#[derive(Debug)]
enum Color {
    Blue,
    Green,
    Yellow,
    Red,
    Magenta
}

#[derive(Debug)]
struct Segment {
    treshold: i8,
    color: Color
}

#[derive(Debug)]
pub struct SimpleMeter {
    display_count: usize,
    hold_period: Duration,
    current_values: [f32; 32],
    timestamps: [Instant; 32],
    segments: Vec<Segment>,
    stdout: Stdout
}

fn draw_segment(stdout: &mut Stdout, segment: &Segment, db_level: i8) -> CrosstermResult<()> {
    // If above treshold, render segment in the current line.
    if db_level >= segment.treshold {
        let _res: CrosstermResult<()> = match segment.color {
        Color::Blue => queue!(stdout, PrintStyledContent("█".blue())),
        Color::Green => queue!(stdout, PrintStyledContent("█".green())),
        Color::Yellow => queue!(stdout, PrintStyledContent("█".yellow())),
        Color::Red => queue!(stdout, PrintStyledContent("█".red())),
        Color::Magenta => queue!(stdout, PrintStyledContent("█".magenta()))
        };
    }
    Ok(())
}

#[allow(dead_code)]
impl SimpleMeter {

    pub fn new(display_count: usize) -> Self {
        SimpleMeter {
            display_count: display_count,
            hold_period: Duration::from_millis(500),
            current_values: [0.0; 32],
            timestamps: [Instant::now(); 32],
            segments: vec![
                Segment { treshold: -72, color: Color::Blue },
                Segment { treshold: -60, color: Color::Blue },
                Segment { treshold: -56, color: Color::Blue },
                Segment { treshold: -48, color: Color::Blue },
                Segment { treshold: -36, color: Color::Blue },
                Segment { treshold: -30, color: Color::Blue },
                Segment { treshold: -24, color: Color::Green },
                Segment { treshold: -18, color: Color::Green },
                Segment { treshold: -15, color: Color::Green },
                Segment { treshold: -12, color: Color::Green },
                Segment { treshold: -9, color: Color::Yellow },
                Segment { treshold: -6, color: Color::Yellow },
                Segment { treshold: -3, color: Color::Red },
                Segment { treshold: -0, color: Color::Magenta },
            ],
            stdout: stdout()
        }
    }

    pub fn update(&mut self, channel_index: usize, value: f32) -> CrosstermResult<()> {
        if value > self.current_values[channel_index] ||
        Instant::now() > self.timestamps[channel_index] + self.hold_period
        {
            self.current_values[channel_index] = value;
            self.timestamps[channel_index] = Instant::now();
        }
        let channel_number = (channel_index + 1) as u16;
        queue!(self.stdout,
            cursor::MoveTo(0, channel_number + 3),  // Start output below line 3 (todo: make a property)
            Print("CH "),
        )?;
        if channel_number < 10 {
            queue!(self.stdout,
                Print("0"),
            )?;
        }
        queue!(self.stdout,
            Print(channel_number),
            Print(": "),
            Clear(ClearType::UntilNewLine),
        )?;
        let db_level = (20.0 * self.current_values[channel_index].log10()) as i8;
        for segment in self.segments.iter() {
            let _res = draw_segment(&mut self.stdout, segment, db_level);
        }
        Ok(())
    }
}

#[allow(dead_code)]
fn main() {
    let mut meter = SimpleMeter::new(2);
    let _res = meter.update(0, 0.12345);
    let _res = meter.update(1, 0.98765);
    println!("{:?}", meter);
}
