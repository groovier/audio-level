extern crate anyhow;
extern crate cpal;
extern crate hound;

use std::{io::{stdout, Write}};
use std::time::{Duration};

use cpal::traits::{DeviceTrait, EventLoopTrait, HostTrait};
use crossterm::{
    cursor::{MoveDown, MoveToColumn},
    terminal::{Clear, ClearType, disable_raw_mode, enable_raw_mode},
    queue,
    cursor,
    event::{poll, read, Event, KeyCode},
};

mod simple_meter;
use simple_meter::SimpleMeter;

fn main() -> Result<(), anyhow::Error> {
    // Use the default host for working with audio devices.
    let host = cpal::default_host();
    // Initialize screen.
    let mut stdout = stdout();
    let _res = queue!(stdout, Clear(ClearType::All), cursor::MoveTo(0, 0),);
    println!("Audio Level - audio peak level indicator");
    // Setup the default input device and stream with the default input format.
    let device = host.default_input_device().expect("Failed to get default input device");
    println!("Input device: {}", device.name()?);
    println!("Press ESC to quit");
    let format = device.default_input_format().expect("Failed to get default input format");
    let number_of_channels: usize = format.channels as usize;
    enable_raw_mode()?;
    let mut meter = SimpleMeter::new(number_of_channels);
    let event_loop = host.event_loop();
    let stream_id = event_loop.build_input_stream(&device, &format)?;
    event_loop.play_stream(stream_id)?;

    std::thread::spawn(move || {
        event_loop.run(move |id, event| {
            let data = match event {
                Ok(data) => data,
                Err(err) => {
                    eprintln!("an error occurred on stream {:?}: {}", id, err);
                    return;
                }
            };
            match data {
                cpal::StreamData::Input { buffer: cpal::UnknownTypeInputBuffer::F32(buffer) } => {
                    let mut values:[f32; 32] = [0.0; 32];
                    let mut current_channel = 0;
                    for &sample in buffer.iter() {
                        // Each value belongs to a new channel until we reach number of channels.
                        if current_channel == number_of_channels {
                            current_channel = 0;
                        }
                        // Read all values from buffer; put high values in new_values.
                        if &sample.abs() > &values[current_channel] {
                            values[current_channel] = sample.abs();
                        }
                        current_channel = current_channel + 1;
                    }
                    // Rendering of each channel separately
                    for channel_index in 0..number_of_channels {
                        let _res = meter.update(channel_index, values[channel_index]);
                    }
                    // Move the cursor to the line below the meter.
                    let _res = queue!(stdout, MoveDown(1), MoveToColumn(0));
                    let _res = stdout.flush();
                },
                _ => (),
            }
        });
    });

    loop {
        // Wait up to 1s for another event
        if poll(Duration::from_millis(1_000))? {
            // It's guaranteed that read() wont block if `poll` returns `Ok(true)`
            let event = read()?;
            if event == Event::Key(KeyCode::Esc.into()) {
                disable_raw_mode()?;
                println!("Bye...");
                break;
            }
        }
    }
    Ok(())
}
